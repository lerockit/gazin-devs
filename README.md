# GazinDevs
Sistema desenvolvido a partir das propostas do teste Potential CRUD

A arquitetura de pastas esta dividida inicialmente em:
- Back-end (API): api/
- Front-end : client/

## **API**
Para inicialização da API é necessário que na maquina ambiente tenha **Docker** instalado e atualizado na última versão.

Para realizar as configurações de ambiente é necessario que se crie o arquivo **.env** a partir do arquivo **.env.example** 

Após ambiente configurado, para subir a imagem do docker usar o comando:

```
docker-compose up -d
```

Assim, com a imagem rodando, instalaremos os pacotes utilizando composer, com o seguinte comando:

```
docker-compose exec app composer install
```

Feito, é necessário a criação das tabelas do banco de dados, para isso, rodar o comando:

```
docker-compose exec app php artisan migrate
```

*(OPCIONAL)*

Criado as tabelas, a aplicação ja está pronta para uso, porem, caso deseje popular o banco com dados ficticios, rodar o comando:


```
docker-compose exec app php artisan db:seed
```

## **Client**

Para inicialização do Client é necessário que na maquina ambiente tenha **node/npm** instalado e atualizado na última versão.

Para realizar as configurações de ambiente é necessario que se crie o arquivo **.env** a partir do arquivo **.env.example** onde a variavel de endereço da API deverá apontar para o endereço onde a API está hosteada

Após ambiente configurado, é necessario a instalação dos pacotes de dependencia através do **npm**, logo:

```
npm i
```

Pacotes instalados já estamos pronto para rodar a aplicação, logo:

```
npm run serve
```