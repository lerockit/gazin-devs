## **API**
Para inicialização da API é necessário que na maquina ambiente tenha **Docker** instalado e atualizado na última versão.

Para realizar as configurações de ambiente é necessario que se crie o arquivo **.env** a partir do arquivo **.env.example** 

Após ambiente configurado, para subir a imagem do docker usar o comando:

```
docker-compose up -d
```

Assim, com a imagem rodando, instalaremos os pacotes utilizando composer, com o seguinte comando:

```
docker-compose exec app composer install
```

Feito, é necessário a criação das tabelas do banco de dados, para isso, rodar o comando:

```
docker-compose exec app php artisan migrate
```

*(OPCIONAL)*

Criado as tabelas, a aplicação ja está pronta para uso, porem, caso deseje popular o banco com dados ficticios, rodar o comando:


```
docker-compose exec app php artisan db:seed
```