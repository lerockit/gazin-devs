<?php

namespace App\Http\Controllers;

use App\Http\Requests\DevelopersRequest;
use App\Http\Resources\DeveloperResource;
use App\Models\Developer;
use Illuminate\Http\Request;

class DevelopersController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->query('per_page') ?? 5;

        $search = $request->query('search') ?? '';

        $developers = Developer::orderBy('name')
            ->filter($search)
            ->paginate($perPage);

        return DeveloperResource::collection($developers);
    }

    public function store(DevelopersRequest $request)
    {
        $data = $request->validated();

        $developer = Developer::create($data);

        return new DeveloperResource($developer);
    }

    public function show(Developer $developer)
    {
        return new DeveloperResource($developer);
    }

    public function update(DevelopersRequest $request, Developer $developer)
    {
        $data = $request->validated();

        $developer->fill($data)->save();

        return new DeveloperResource($developer);
    }

    public function destroy($id)
    {
        $developer = Developer::find($id);

        $developer->delete();

        return response()->noContent();
    }
}
