<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DevelopersRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'gender' => ['required', 'in:f,m'],
            'hobby' => ['required', 'string'],
            'birthdate' => ['required', 'date'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Insira o campo nome',
            'name.string' => 'Insira um valor válido ao campo nome',
            'gender.required' => 'Insira o campo sexo',
            'gender.in' => 'O campo sexo deve ser "Masculino" ou "Feminino"',
            'hobby.required' => 'Insira o campo hobby',
            'hobby.string' => 'Insira um valor válido ao campo hobby',
            'birthdate.required' => 'Insira o campo data de nascimento',
            'birthdate.date' => 'Insira uma data de nascimento válida',
        ];
    }
}
