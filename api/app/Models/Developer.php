<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $fillable = [
        'name',
        'gender',
        'age',
        'hobby',
        'birthdate',
    ];

    protected $appends = ['age'];

    public function scopeFilter($query, string $search) 
    {
        if(!empty($search)) {
            $query->where('name', 'like', '%'.$search.'%');
        }
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['birthdate'])->age;
    }
}
