<?php

use App\Models\Developer;
use Faker\Generator as Faker;

$factory->define(Developer::class, function (Faker $faker) {
    $date = $faker->dateTimeBetween('-50 years', 'now');
    return [
        'name' => $faker->name,
        'gender' => $faker->randomElement(['f', 'm']),
        'hobby' => $faker->randomElement(['Jogar', 'Dançar', 'Estudar', 'Desenvolver']),
        'birthdate' => $date->format('Y-m-d')
    ];
});
