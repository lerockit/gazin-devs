<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopersTable extends Migration
{
    public function up()
    {
        Schema::create('developers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->char('gender', 1);
            $table->string('hobby');
            $table->date('birthdate');
        });
    }

    public function down()
    {
        Schema::dropIfExists('developers');
    }
}
