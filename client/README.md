## **Client**

Para inicialização do Client é necessário que na maquina ambiente tenha **node/npm** instalado e atualizado na última versão.

Para realizar as configurações de ambiente é necessario que se crie o arquivo **.env** a partir do arquivo **.env.example** onde a variavel de endereço da API deverá apontar para o endereço onde a API está hosteada

Após ambiente configurado, é necessario a instalação dos pacotes de dependencia através do **npm**, logo:

```
npm i
```

Pacotes instalados já estamos pronto para rodar a aplicação, logo:

```
npm run serve
```