import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from '@/router'
import VueSnackbar from 'vue-snack'
import snackConfig from '@/services/snack'
import Buefy from 'buefy'
import { dateFilter, genderFilter } from '@/services/filters'
import Vuelidate from 'vuelidate'
import vueDebounce from 'vue-debounce'

import '@/assets/styles/main.scss'
import '@mdi/font/scss/materialdesignicons.scss'

Vue.use(VueRouter)
Vue.use(VueSnackbar, snackConfig)
Vue.use(Buefy)
Vue.use(Vuelidate)
Vue.use(vueDebounce)

Vue.filter('date', dateFilter)
Vue.filter('gender', genderFilter)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
