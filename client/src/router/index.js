import VueRouter from 'vue-router'
import routes from '@/router/routes'

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.afterEach((to) => {
  document.title = to.meta.title ? `${to.meta.title} | GDevs` : 'GDevs'
})

export default router