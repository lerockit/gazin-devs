import DevelopersIndex from '@/views/developers/Index'
import DevelopersCreate from '@/views/developers/Create'
import DevelopersEdit from '@/views/developers/Edit'
import DevelopersView from '@/views/developers/View'

export default [
  {
    path: '/',
    name: 'index',
    redirect: { name: 'developers.index' }
  },
  {
    path: '/developers',
    name: 'developers.index',
    component: DevelopersIndex,
    meta: { title: 'Desenvolvedores' },
  },
  {
    path: '/developers/create',
    name: 'developers.create',
    component: DevelopersCreate,
    meta: { title: 'Adicionar Desenvolvedor' },
  },
  {
    path: '/developers/edit/:developerId',
    name: 'developers.edit',
    component: DevelopersEdit,
    meta: { title: 'Editar Desenvolvedor' },
  },
  {
    path: '/developers/view/:developerId',
    name: 'developers.view',
    component: DevelopersView,
    meta: { title: 'Desenvolvedor' },
  },
]