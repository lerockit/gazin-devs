import moment from 'moment'

export const dateFilter = (val) => moment(val).format('DD/MM/YYYY')

export const genderFilter = val => val === 'm' ? 'Masculino' : 'Feminino'