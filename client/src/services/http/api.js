import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

axiosInstance.interceptors.request.use(config => {
  config.headers['Accept'] = 'application/json'
  return config
});

export default axiosInstance
