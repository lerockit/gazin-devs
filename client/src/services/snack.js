export default {
  position: 'bottom-right', 
  time: 3000,
  methods: [
    { name: 'success', color: '#48BB78' },
    { name: 'error', color: '#F56565' },
    { name: 'warning', color: '#ECC94B' },
  ]
}