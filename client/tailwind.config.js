module.exports = {
  purge: [],
  theme: {
    extend: {},
    container: {
      center: true
    },
    fontFamily: {
      display: ['Raleway', 'sans-serif'],
      body: ['Lato', 'sans-serif']
    }
  },
  variants: {},
  plugins: [],
}
